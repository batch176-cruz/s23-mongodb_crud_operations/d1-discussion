db.products.insertOne({

    

    "name": "Iphone X",

    "price": 25000

    

})



db.products.insertMany([

    {

        name: "Alcohol",

        price: 35

    },

    {

        name: "Face Mask",

        price: 10

    }  

])


db.products.find()
db.products.findOne({})
db.products.findOne({name: "Face Mask"})
db.products.find({price: 35})





// Update

db.products.updateOne({name: "Face Mask"},{$set:{price:25}})

// If criteria is empty in update, it will search for all items in the collection.

// If the field being updated does not exist, mongoDB will create that field instead.

db.products.updateMany({},{$set:{isActive:true}})

// If the search criteria for updateOne is empty, it will search the first item in the collection

db.products.updateOne({},{$set:{isActive:false}})





// Delete

db.products.deleteOne({name: "Alcohol"})

// deleteOne({"field":"value"}) - deletes the first item that matches the criteria

db.products.deleteMany({isActive:false})

// deleteMany({"field":"value"})









db.products.insertMany([

    {

        name: "Samsung Galaxay s21",

        price: 35000,

        isActive: true

    },

    {

        name: "Headphones",

        price: 2500,

        isActive: true

    },

    {

        name: "Headphones",

        price: 3500,

        isActive: false

    }

])

    

    

// deleteOne

db.products.deleteOne({name: "Headphones", price: 3500})

db.products.deleteOne({})

db.products.deleteMany({})