// CRUD OPERATIONS

// Create - Insert Documents
/*
	Syntax:
		Inserting One Document

		-db.collectionName.insertOne({
				"fieldNameA": "valueA",
				"fieldNameB": "valueB"
		})

*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
})

/*
	Insert Many Documents

	Syntax:
		-db.collectionName.inserMany([
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			},
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			}
		])
*/

db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"email": "stephenhawking@mail.com",
		"department": "none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@mail.com",
		"department": "none"
	}
])

/*
	Mini Activity:
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

		name: JavaScript 101,
		price: 5000,
		description: Introduction to JavaScript,
		isActive: true

		name: HTML 101,
		price: 2000,
		description: Introduction to HTML,
		isActive: true

		name: CSS 101,
		price: 2500,
		description: Introduction to CSS,
		isActive: false
*/

db.courses.insertMany([
	{
		"name": "JavaScript 101",
		"price": "5000", 
		"description": "Introduction to JavaScript",
		"isActive": true
	},
	{
		"name": "HTML 101",
		"price": "2000", 
		"description": "Introduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": "2500", 
		"description": "Introduction to CSS",
		"isActive": false
	}
])

// Read - Find Documents
/*
	Syntax:
		-db.collectionName.find() - this will retrieve all our documents
		-db.collectionName.find({"criteria": "value"}) - will retrieve all documents that match our criteria
		-db.collectionName.findOne({"criteria": "value"}) - will return the first document in our collection that match our criteria
		?? -db.collectionName.find({}) - will return the first document in our collection
		-db.collectionName.findOne({}) - will return the first document
*/

db.users.find()
db.users.findOne({})
db.users.find({
	"lastName": "Armstrong",
	"age": 82
})

// Update Documents
/*
	Syntax:
		db.collectionName.updateOne(
		{
			"criteria": "field"
		},
		{
			$set: {
				"fieldToBeUpdate": "updatedValue"
			}
		})

		db.collectionName.updateMany(
		{
			"criteria": "field"
		},
		{
			$set: {
				"fieldToBeUpdate": "updatedValue"
			}
		})
*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"department": "none"
})

db.users.updateOne(
	{
		"firstName": "Test"
	},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations",
			"status": "active"
		}
	}
)

db.users.updateMany(
	{
		"department": "none"
	},
	{
		$set: {
			"department": "HR"
		}
	}
)

// Removing a field
db.users.updateOne(
	{
		"firstName": "Bill"
	},
	{
		$unset: {
			"status": "active"
		}
	}
)

// Mini Activity
/*
	1. Update the HTML 101 Course
		- Make the isActive to false

	2. Add enrollees field to all documents in our courses collection
		- Enrollees: 10


*/

db.courses.updateOne(
	{
		"name": "HTML 101"
	},
	{
		$set: {
			"isActive": false
		}
	}
)

db.courses.updateMany({},
	{
		$set: {
			"enrollees": 10
		}
	}
)














//

db.products.insertOne({
    
    "name": "Iphone X",
    "price": 25000
    
})

db.products.insertMany([
    {
        name: "Alcohol",
        price: 35
    },
    {
        name: "Face Mask",
        price: 10
    }  
])